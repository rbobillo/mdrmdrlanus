Consignes

Faire un programme permettant de generer des melanges aleatoires pour Rubiks cube

Les mouvements autorises sont: Up, Down, Right, Left, Front, Back
Ils sont representes par la premiere lettre de leur nom: U,D,R,L,F,B

Chaque mouvement peut avoir 3 methodes de rotation:
  - Rotation horaire (ex: R)
  - Rotation anti-horaire (ex: R')
  - Rotation double (ex: R2)

Le programme sera execute en passant comme argument, le nombre de mouvements aleatoires a generer.

Exemple:
```
$> java -jar scramble.jar 5
$> B U' D2 F L'
$> java -jar scramble.jar mdr
$> Cannot Scramble
```

Par souci de simplicite et d'efficacite, voici quelques regles supplementaires:
  - un seul argument sera passe en parametre, il devra etre un nombre entier
  - la complexite algorithmique ne devra pas exceder O(N^2)
  - 2 mouvements identiques peuvent se succeder
